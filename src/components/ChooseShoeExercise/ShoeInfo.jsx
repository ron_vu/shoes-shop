import React, { Component } from "react";

export default class ShoeInfo extends Component {
  render() {
    const shoeChoose = this.props.shoeChoose;
    return (
      <div id="showDetail">
        <header className="titleDetail">
          <h1>Thông tin chi tiết</h1>
        </header>
        <main className="bodyDetail">
          <img id="prodImg" src={shoeChoose.image} alt />
          <p id="prodName">
            <span>Tên:</span> {shoeChoose.name}
          </p>
          <p id="prodPrice">
            <span>Giá:</span> {shoeChoose.price}
          </p>
          <p id="prodDesc">
            <span>Mô tả:</span>
            {shoeChoose.description}
          </p>
          <p id="prodQuantity">
            <span>Số lượng:</span> {shoeChoose.quantity}
          </p>
        </main>
      </div>
    );
  }
}
