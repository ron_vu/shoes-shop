import React, { Component } from "react";

export default class ShoeItem extends Component {
  handleClickItem = () => {
    const shoeItem = this.props.shoeItem;
    this.props.handleChangeShoe(shoeItem);
  };

  //Cách truyển tham số 
//   handleClickItem = (item) => {
//     this.props.handleChangeShoe(item);
//   };

  render() {
    const shoeItem = this.props.shoeItem;
    // console.log(shoeItem);
    return (
      // cách dùng k cẩn truyển tham số
      <div className="productContent" >
        <img
          src={shoeItem.image}
          alt="shoe-image"
          onClick={this.handleClickItem}
          //   onClick={()=>{this.handleClickItem(this.props.shoeItem)}}  //Cách truyển tham số vào hàm onClick
        />
        <p className="nameProduct" onClick={this.handleClickItem}>{shoeItem.name} </p>
        <span className="price">{shoeItem.price}</span>
        <button id="btnThemGioHang" className="btn btn-success">
          Add to Carts <i className="fa fa-cart-plus" />
        </button>
      </div>
    );
  }
}
