import logo from "./logo.svg";
import "./App.css";
import "./ui/style.css";
import Header from "./components/Header";
import Navigation from "./components/Navigation";
import ExampleLayout from "./components/ExampleLayout";
import LearningDataBinding from "./components/LearningDataBinding";
import ExampleShowRoom from "./components/ExampleShowRoom";
import ChooseGlassExcerise from "./components/ChooseGlassExercise";
import ShoesStore from "./components/ChooseShoeExercise";

// JSX : html + js
function App() {
  return (
    <div>
      {/* <LearningDataBinding></LearningDataBinding>
      <ExampleLayout></ExampleLayout>
      <ExampleShowRoom /> */}
      {/* <ChooseGlassExcerise></ChooseGlassExcerise> */}
      <ShoesStore></ShoesStore>
    </div>
  );
}

export default App;
